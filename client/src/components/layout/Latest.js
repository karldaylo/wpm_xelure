import React, { Component } from 'react';
import $ from 'jquery';
// import e from 'express';

class Latest extends Component{

    componentDidMount() {
        
        function getmeList() {
            return fetch('http://localhost:5000/history/latest')
            .then((response) => response.json())
            .then((responseJson) => {
                //text_array.push(responseJson[0]);
                var responseText = responseJson;
                responseJson.forEach(obj => {

                    var parserow = "<tr><td>"+obj['nickname']+"</td><td>"+obj['wpm']+"</td><td>"+obj['accuracy']+"</td><td>"+obj['total_words']+"</td><td>"+obj['date']+"</td></tr>";
                    $("#tablecontent").append(parserow);
                    
                });
                
            })
            .catch((error) => {
              console.error(error);
            });
        }
        getmeList();
        $("#nav-latest").addClass("active");

        //learn maping later
    }

    render() {
        return (
            <div className="container dashboard-container">
                <div className="table-responsive">
                    <table className="table" id="table">
                        <thead>
                            <tr>
                              <th className="text-center tbhead" colSpan="5">Latest Entries</th>
                            </tr>
                            <tr>
                              <th>Challenger</th>
                              <th>Average WPM</th>
                              <th>Accuracy %</th>
                              <th>Word Count</th>
                              <th >Date</th>
                            </tr>
                          </thead>
                          <tbody id="tablecontent">
                          </tbody>
                    </table>
                </div>
            </div>
            
        );
    }
}

export default Latest;