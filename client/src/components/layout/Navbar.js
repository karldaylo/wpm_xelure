import React, { Component } from 'react'

class Navbar extends Component{
    render() {
        return (
            <nav className="navbar navbar-static-top navbar-default" >    
                <div className="container">
                    <div className="navbar-header">
                        
                        <a className="navbar-brand" href="#">Swords Per Minute</a>
                    </div> 
                    
                    
                    <ul className="nav navbar-nav">
                        <li id="nav-play">		<a href="/"> <span className="glyphicon glyphicon-play"></span> Play</a></li>
                        <li id="nav-leaderboard">		<a href="/leaderboard"> <span className="glyphicon glyphicon-list"></span> Leaderboards</a></li>
                        <li id="nav-latest">		<a href="/latest"> <span className="glyphicon glyphicon-file"></span> &nbsp; 	Latest Activities</a></li>
                        <li id="nav-history">		<a href="#"> <span className="glyphicon glyphicon-time"></span> &nbsp; 	My History</a></li>
                    </ul>
                    

                    <ul className="nav navbar-nav navbar-right">	        	
                        <li className="dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button">
                            <span className="glyphicon glyphicon-user"></span> &nbsp;&nbsp; Login <span className="caret"></span></a>
                            
                        </li>
                    </ul>		      	
                </div>
            </nav>
        );
    }
}

export default Navbar;