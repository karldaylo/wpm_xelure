import React, { Component } from 'react';
import axios from 'axios';
import $ from 'jquery';
// import e from 'express';

class Racebox extends Component{

    componentDidMount() {
        var mainchar_animation_state = 0;
        var enemychar_animation_state = 0;
        /*State natin
            0 - idle
            1 - standby
            2 - hit
            3 - attack
            4 - attack fast
            5 - attack faster
            6 - reserve ko muna
            7 - win
            8 - win looped
            9 - lose
        */
        var playing = 0;
        var second_ran = 0;
        var second_end = 0;
        var minutes_end = 0;
        var word_count = 0;
        var word_iteration = 0;
        var incorrect_count = 0;
        var inputted_count = 0;
        var expected_wpm = 0;
        var total_wpm = 0;
        var total_accuracy = 0;
        var target_text = [];
        var completed_text = "";
        var cursor_text = "";
        var error_text = "";
        var remaining_text = "";
        //initiate
        function getRandomParagraph() {
            return fetch('https://baconipsum.com/api/?type=meat-and-filler&sentences=3')
            .then((response) => response.json())
            .then((responseJson) => {
                //text_array.push(responseJson[0]);
                var responseText = responseJson[0].toLowerCase();
                $("#highlight-result").text(responseText);
                $("#target-texts").val(responseText);
                $("#activity-box").val("");
                remaining_text = responseText;
                target_text = responseText.split("");
                var target_words = responseText.split(" ");

                //initiate cursor
                word_count = target_words.length;
                console.log("word_count:"+word_count);
                cursor_text = target_text[0];
                remaining_text = remaining_text.substring(1);
                $("#redtext").text(cursor_text);
                $("#highlight-result").text(remaining_text);
            })
            .catch((error) => {
              console.error(error);
            });
        }
        //function muna baka magamit ulit
        getRandomParagraph();
        $("#nav-play").addClass("active");
        

        
        
        // function submitHistory2() {
        //     console.log("history submit");
        //     const requestOptions = {
        //         method: 'POST',
        //         headers: { 'Content-Type': 'application/json'},
        //         body: JSON.stringify(
        //             { nickname: 'Guest',//since not yet implemented
        //               total_words: word_count,
        //               total_second: second_end,
        //               total_minute: minutes_end,
        //               wpm: total_wpm,
        //               accuracy: total_accuracy,
        //             }
        //         )
        //     };
        //     console.log(requestOptions);
        //     axios.post('http://localhost:5000/history/test', requestOptions)
        //     .then(res => {
        //         console.log(res);
        //       })
        //     .catch((error) => {
        //         console.log(error)
        //         alert(error);
        //     });
        // }

        function submitHistory() {
            console.log("history submit");
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json'},
                body: JSON.stringify(
                    { nickname: 'Guest',//since not yet implemented
                      total_words: word_count,
                      total_second: second_end,
                      total_minute: minutes_end,
                      wpm: total_wpm,
                      accuracy: total_accuracy,
                    }
                )
            };
            console.log(requestOptions);
            return fetch('http://localhost:5000/history/save', requestOptions)
            .then((response) => response.json())
            // .then(data => this.setState({ postId: data.id }))
            .then((responseJson) => {
                console.log("history saved");
                console.log(responseJson);
            })
            .catch((error) => {
                console.log(error)
                // alert(error);
            });
        }

        // submitHistory();


        function changeAnimation(id,path){
            var image = new Image();
            path = "../img/"+path+".gif";
            image.src = path;
            $("#"+id).attr('src', image.src);
        }

        function attackAnimation(){
            //already attacking so do nothing
            if(mainchar_animation_state >= 3 && mainchar_animation_state <= 6){
                return;
            }

            if(expected_wpm >= 50){
                changeAnimation("mainchar","sm_attack_fast");
                mainchar_animation_state = 3;
            }else if (expected_wpm <= 51 &&expected_wpm >= 100){
                changeAnimation("mainchar","sm_attack_faster");
                mainchar_animation_state = 4;
            }else{
                changeAnimation("mainchar","sm_attack");
                mainchar_animation_state = 5;
            }

            //rest after
            // setInterval(function(){
            //     changeAnimation("mainchar","sm_standby");
            //     mainchar_animation_state = 1;
            // }, 100);

            var restToIdle = function() {
                var restToIdleTimer = setTimeout(restToIdle, 1000);
                changeAnimation("mainchar","sm_standby");
                mainchar_animation_state = 1;
                clearTimeout(restToIdleTimer);
            };


            restToIdle = setTimeout(restToIdle, 1000);
        
        }

        function enemy_attackAnimation(){
            //already attacking so do nothing
            if(enemychar_animation_state == 3){
                return;
            }

            
            changeAnimation("enemychar","dk_attack");
            enemychar_animation_state = 3;

            //rest after
            // setInterval(function(){
            //     changeAnimation("mainchar","sm_standby");
            //     mainchar_animation_state = 1;
            // }, 100);

            var restToIdle = function() {
                var restToIdleTimer = setTimeout(restToIdle, 500);
                changeAnimation("enemychar","dk_idle");
                enemychar_animation_state = 1;
                clearTimeout(restToIdleTimer);
            };

            restToIdle = setTimeout(restToIdle, 500);
        
        }

        function calculateWPM(words,seconds){
            var quotient = Math.floor(seconds/60);
            var remainder = seconds % 60;
            var minutes = parseFloat(quotient.toString()+"."+remainder.toString()); //lol
            minutes_end = minutes;

            var wpm = Math.floor(words/minutes);

            console.log("seconds:"+seconds);
            console.log("min:"+minutes);
            console.log("words:"+words);
            console.log("wpm:"+wpm);
            return wpm;
        }

        function calculateAccuracy(incorrect,total){
            var correct     = total - incorrect;
            var accuracy    = (correct / total) * 100;
            
            return accuracy.toFixed(2);;
        }


        function keypressed(){
            if(!$("#activity-box").val()){
                //if empty skip;
                return;
            }

            if(!remaining_text){
                //do something
                if(second_end==0){
                    //just incase
                    second_end = second_ran;
                }
                total_wpm = calculateWPM(word_count,second_end);
                total_accuracy = calculateAccuracy(incorrect_count,inputted_count);
                cursor_text = " ";



                //temp
                //save history
                submitHistory();
                if(!alert(
                    "WPM:"+total_wpm+"\n"
                    +"Accuracy:"+total_accuracy+"\n"
                    +"Word Count:"+word_count+"\n"
                    +"\n"+"History Saved... Click okay for new game."
                )){window.location.reload();}

                return;
            }

            if(playing == 0){
                console.log("playing...");
                playing = 1;

                
                // setInterval(addSecond, 1000);

                setInterval(function(){
                    second_ran++;
                    if(second_ran>2){
                        expected_wpm = calculateWPM(word_count,second_ran);
                    }
                }, 1000);
            }

            // console.log("target:"+target_text[word_iteration]);
            // console.log("input:"+$("#activity-box").val());

            inputted_count++;
            //check
            if($("#activity-box").val() == target_text[word_iteration]){
                //success
                // console.log("result:success");

                completed_text = completed_text.concat(target_text[word_iteration]);
                remaining_text = remaining_text.substring(1);
                word_iteration++;
                cursor_text = target_text[word_iteration];
                if(cursor_text == " "){
                    cursor_text = "_";
                }

                //animate
                attackAnimation();
            }else{
                //error
                //console.log("result:failed");
                incorrect_count++;

                //animate
                enemy_attackAnimation();
            }

            // console.log(remaining_text);
            $("#bluetext").text(completed_text);
            $("#redtext").text(cursor_text);
            $("#highlight-result").text(remaining_text);
            //end it;
            $("#activity-box").val("");
        }

        //listener
        $("#activity-box").keydown(function(){
            keypressed();
        });
        $("#activity-box").keyup(function(){
            keypressed();
        });

        //prevent highlight for magic
        var inp = document.getElementById('activity-box');
        inp.addEventListener('select', function() {
          this.selectionStart = this.selectionEnd;
        }, false);
        
        //force focus
        $("#activity-box").focus();
        $("#activity-box").blur(function(){
            $("#activity-box").focus();
        });

    }

    render() {
        return (
            <div className="container dashboard-container">
                <div className="col-md-12 text-center">
                    <img id="mainchar" src="../img/sm_idle.gif" />
                    <img id="enemychar" src="../img/dk_idle.gif" />
                    <img id="grassland" src="../img/grassland.png" />
                </div>
                
                <div className="col-md-12 text-left">
                    <textarea id="activity-box" spellCheck="false"></textarea>
                    <div id="dialog">
                        <span id="bluetext"></span>
                        <span id="redtext"></span>
                        <span id="highlight-result">Loading...</span>
                        <input type="hidden" value="" id="target-texts"/>
                    </div>
                </div>
                <div className="preload">
                    <img src="../img/sm_attack.gif" />
                    <img src="../img/sm_attack_fast.gif" />
                    <img src="../img/sm_attack_faster.gif" />
                    <img src="../img/sm_hit.gif" />
                    <img src="../img/sm_idle.gif" />
                    <img src="../img/sm_move.gif" />
                    <img src="../img/sm_standby.gif" />
                    <img src="../img/sm_win.gif" />
                    <img src="../img/sm_win_loop.gif" />
                    <img src="../img/dk_attack.gif" />
                    <img src="../img/dk_dead.gif" />
                    <img src="../img/dk_hit.gif" />
                    <img src="../img/dk_idle.gif" />
                    <img src="../img/dk_move.gif" />
                    <img src="../img/dk_standby.gif" />
                </div>
            </div>
            
        );
    }
}

export default Racebox;