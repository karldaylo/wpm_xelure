import React, { Component } from 'react'
import $ from 'jquery';
import e from 'express';

class Playfield extends Component{
    initiate frontend jquery voodoo
    constructor() {
        function getRandomParagraph() {
            return fetch('https://baconipsum.com/api/?type=meat-and-filler&sentences=5')
            .then((response) => response.json())
            .then((responseJson) => {
              //text_array.push(responseJson[0]);
              console.error(responseJson[0]);
            })
            .catch((error) => {
              console.error(error);
            });
        }
        var target_msg = getRandomParagraph();
        $("#highlight-result").text(target_msg);
        $("#target-texts").val(target_msg);

    }

    componentDidMount(){
        //get it
        
        
        var iterate = 0;

        //game state to zero
        var playing = false;
        var timer2 = "3:00";
        var interval = null;
        $('#counter').text("3:00");
        
        function timer_base() {
            var timer = timer2.split(':');
            //by parsing integer, I avoid all extra string processing
            var minutes = parseInt(timer[0], 10);
            var seconds = parseInt(timer[1], 10);
            --seconds;
            minutes = (seconds < 0) ? --minutes : minutes;
            if (minutes < 0) clearInterval(interval);
            seconds = (seconds < 0) ? 59 : seconds;
            seconds = (seconds < 10) ? '0' + seconds : seconds;
            //minutes = (minutes < 10) ?  minutes : minutes;
            $('#counter').text(minutes + ':' + seconds);
            timer2 = minutes + ':' + seconds;
        }

        function begin_timer(){
            var interval = setInterval(timer_base, 1000);
        }
        
        function reset_timer(){
            clearInterval(timer_base);
            var timer2 = "3:00";
            $('#counter').text("3:00");
            interval = null;
        }

        $("#reset-btn").click(function(){
            reset_timer();
            playing = false;
        });


        $("#inputzone").keypress(function(){
            
            //begin game if new
            if(!playing){
                begin_timer();
                playing = true;
            }

            var target_text = $("#target-texts").val();
            var myinput = $("#inputzone").val(); 

            //check content
            //if done reset
            if(target_text == myinput){
                alert("debug:next activity");
            }else{
                
            }
            //if not done do more
        });

        

    }
    render() {

        

        return (
            <div className="container dashboard-container">
                <div className="col-md-12 text-center">
                    <a className="btn btn-danger " id="reset-btn">Reset</a><br></br>
                    <span id="counter">3:00</span> <br/>
                </div>
                <div className="col-md-6">
                    <fieldset>
                        <legend>Target Message</legend>
                        <div id="highlight-result">Error</div>
                        <input type="hidden" value="" id="target-texts"/>
                    </fieldset>
                </div>
                <div className="col-md-6">
                    <fieldset>
                        <legend>Your Input</legend>
                        <div className="form-group">
                            
                            <textarea className="form-control" id="inputzone" rows="3" placeholder="Type here to start the game timer..."></textarea>
                        </div>
                        
                    </fieldset>
                </div>
            </div>
            
        );
    }
}

export default Playfield;