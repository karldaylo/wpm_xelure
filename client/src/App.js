import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import Navbar from './components/layout/Navbar';
import Racebox from './components/layout/Racebox';
import Latest from './components/layout/Latest';
import Leaderboard from './components/layout/Leaderboard';

import './App.css';

class App extends Component {
	componentDidMount() {

	}
  	render(){
    	return(
	      <div className="App">
	      <Router>
	        <Navbar></Navbar>
	        <Route exact path="/" component={Racebox} />
	        <Route exact path="/leaderboard" component={Leaderboard} />
	        <Route exact path="/Latest" component={Latest} />
	      </Router>
	      </div>
    	);
  	}
}

export default App;
