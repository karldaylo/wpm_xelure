const express 	= require('express');
const connectDB = require('./config/database');
var cors = require('cors')

const app 		= express();
const port 		= process.env.PORT || 5000;

//DB
connectDB();

//middleware
app.use(express.json({extended:false}));
app.use(cors());

//routes
app.use('/account', require('./routes/account'));
app.use('/guest', require('./routes/guest'));
app.use('/history', require('./routes/history'));


app.get('/', (req,res) => res.send("Wassup"));

//run
app.listen(port);