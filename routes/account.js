const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');
const {check, validationResult } = require('express-validator/check');
const User = require('../model/User');

router.post('/test', (req,res) => {
	console.log(req.body);
	res.json(req.body);
});


//@GET /account/register
router.post('/register', 
	[
		check('nickname', 'Required Field').not().isEmpty(),
		check('email', 'Invalid email format').isEmail(),
		// must be at least 5 chars long
		check('password', 'Minimum of 6 characters').isLength({ min: 5 })
		// check('nickname').isLength({ min: 5 })
	],
	async (req, res) => {
		
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {nickname,email,password} = req.body;


    try{
    	//check user if exists
    	let user = await User.findOne({ email });
    	if(user){
    		res.status(400).json({errors: [{msg: 'Email already used'}] });
    	}

 		user = new User({
	      email,
	      password,
	      nickname
	    });

	    //passhash
	    const salt = await bcrypt.genSalt(10);

	    user.password = await bcrypt.hash(password,salt);

	    //save to db
	    await user.save();

	    //return jwt
	    const payload = {
	    	user:{
	    		id: user.id
	    	}
	    };

    	jwt.sign(
    		payload,
    		config.get('jwtpass'),
    		{expiresIn:3600},
    		(err,token)=>{
    			if(err) throw err;
    			res.json({ token });
    		});
    	
    }catch(err){
    	console.error(err.message);
    	res.status(500).send('server error');
    }
    
  },
);

module.exports = router;