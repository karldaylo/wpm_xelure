const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');
const {check, validationResult } = require('express-validator/check');
const History = require('../model/History');

router.post('/test', (req,res) => {
	console.log(req.body);
	res.json(req.body);
});

router.post('/save',
	async (req, res) => {
		
    try{
    	
    	const {nickname,total_words,total_second,total_minute,wpm,accuracy} = req.body;

 		history = new History({
	      nickname,
	      total_words,
	      total_second,
	      total_minute,
	      wpm,
	      accuracy,
	    });

	    //save to db
	    await history.save();


    	res.send('done');

    }catch(err){
    	console.error(err.message);
    	res.status(500).send('server error');
    }
    
  },
);

router.get('/latest',
	async (req, res) => {
    try{
    	let history = await History.find().limit(25).sort({date:-1});

    	res.json(history);

    }catch(err){
    	console.error(err.message);
    	res.status(500).send('server error');
    }
    
  },
);

router.get('/leaderboard',
	async (req, res) => {
    try{
    	let leaderboard = await History.find().limit(25).sort({wpm:-1});

    	res.json(leaderboard);

    }catch(err){
    	console.error(err.message);
    	res.status(500).send('server error');
    }
    
  },
);


module.exports = router;