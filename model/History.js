const mongoose = require('mongoose');

const HistorySchema = new mongoose.Schema({
	nickname: {
		type: String,
		required: true
	},
	username: {
		type: String,
	},
	total_words: {
		type: String,
		required: true
	},
	total_second: {
		type: String,
	},
	total_minute: {
		type: String,
	},
	wpm: {
		type: String,
	},
	accuracy: {
		type: String,
	},
	date: {
		type: Date,
		default: Date.now
	}
});

module.exports = History = mongoose.model('history', HistorySchema);