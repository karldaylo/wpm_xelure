const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
	email: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	nickname: {
		type: String,
		default: "Unkown"
	}
});

module.exports = User = mongoose.model('user', UserSchema);